export interface Todo {
    id: string;
    todo: string;
}

export type TodoList = Todo[];