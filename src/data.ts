import { TodoList } from "./types"

export const data:TodoList = [
    {id: "1", todo: "Invite team members and collaborators"},
    {id: "2", todo: "Create a new merge request"},
    {id: "3", todo: "Automatically close issues from merge requests"},
    {id: "4", todo: "Enable merge request approvals"},
    {id: "5", todo: "Automatically merge when pipeline succeeds"}
];