import React from 'react';

import {
    Experimental_CssVarsProvider as CssVarsProvider,
  } from '@mui/material/styles';

import App from './App';

const AppProvider: React.FC = () => (
    <CssVarsProvider>
        <App />
    </CssVarsProvider>
);

export default AppProvider;