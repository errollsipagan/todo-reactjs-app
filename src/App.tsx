import { CssBaseline, TextField, Typography, useTheme } from "@mui/material";
import { Box, Container } from "@mui/system";
import React, { KeyboardEvent, useState } from "react";
import { makeid } from "./commons";
import CheckboxList from "./components/CheckboxList";
import { data } from "./data";
import { TodoList } from "./types";

const App = () => {
    const theme = useTheme();
    const [todoList, setTodoList] = useState<TodoList>(data);
    const [todo, setTodo] = useState('');

    const handleKeyDown = (event: KeyboardEvent) => {
        if (event.key === 'Enter') {
            setTodoList([...todoList, { id: makeid(8), todo }]);
            setTodo('');
        }
    };

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setTodo(event.target.value);
    };

    return (
        <React.Fragment>
            <CssBaseline />
            <Container maxWidth="sm">
                <Box
                    borderRadius={theme.spacing(2)}
                    sx={{
                        bgcolor: theme.palette.primary.light,
                        height: "100vh",
                        padding: `${theme.spacing(4)} ${theme.spacing(2)}`,
                    }}
                    display="flex"
                    flexDirection="column"
                    justifyContent="space-between"
                >
                    <Box>
                        <Typography variant="h3" sx={{ color: theme.palette.background.paper }} gutterBottom>Todo List:</Typography>
                        <CheckboxList todoList={todoList} />
                    </Box>
                    <Box
                        sx={{
                            bgcolor: theme.palette.background.paper,
                        }}
                    >
                        <TextField label="Enter todo here" variant="filled" fullWidth onKeyDown={handleKeyDown} onChange={handleChange} value={todo} />
                    </Box>
                </Box>
            </Container>
        </React.Fragment>
    );
};

export default App;
